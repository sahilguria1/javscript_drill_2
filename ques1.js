function getMail(data){
    //handle edge cases
    if(data == null || data.length<=0){
        return "provide some data to operate."
    }

    let emails = []; //to store emails in array
    const dataLen = data.length; //length of data list

    //Loop to get the mail from each data list
    for(index = 0; index<dataLen; index++){
        emails[index] = data[index].email;
    }
    return emails;
}
module.exports = getMail;