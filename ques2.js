function getHobbieByAge(data, age) {
  //handle edge cases
  if (data == null || data.length <= 0) {
    return "provide some data to operate.";
  }

  const lenOfData = data.length;

  //check for correct age
  if (age <= 0 || age > 99) {
    return "Enter a valid age.";
  }
  //check for data is not empty
  if (lenOfData == 0) {
    return "Data is empty.";
  }

  const hobbies = [];

  //using for loop to check the age and then putting hobbies in listOfHobbies array
  for (index = 0; index < lenOfData; index++) {
    if (data[index].age == 30) {
      hobbies[hobbies.length] = data[index].hobbies;
    }
  }
  return hobbies;
}
module.exports = getHobbieByAge;
