function getStudentAge(data){
    //handle edge cases
    if (data == null || data.length === 0) {
        return "provide some data to operate.";
    }

    const lenOfData = data.length;


    const studentAge = []; //array if student 

    //using for loop putting age in array studentAge 
    for( index = 0; index<lenOfData; index++){
            studentAge[studentAge.length] = data[index].age;
    }

    //showing meassage if no data
    if(studentAge.length == 0){
       return 'No student exists.';
    }
    return studentAge;

}
module.exports = getStudentAge;