function getNameEmailByAge(data, age) {
  //handle edge cases
  if (data == null || data.length === 0) {
    return "provide some data to operate.";
  }
  const lenOfData = data.length;

  //check for correct age
  if (age <= 0 || age > 99) {
    return console.log("Enter a valid age.");
  }

  //using for loop to check the age and then putting hobbies in listOfHobbies array
  for (index = 0; index < lenOfData; index++) {
    if (data[index].age == age) {
      return `${data[index].name} , ${data[index].email}`;
    }
  }
}
module.exports = getNameEmailByAge;
