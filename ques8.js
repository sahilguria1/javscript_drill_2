function logCityAndCountry(data) {
  //handle edge cases
  if (data == null || data.length === 0) {
    return "provide some data to operate.";
  }
  const lenOfData = data.length;

  const cityAndCountry = [];
  //using for loop to check the age and then putting hobbies in listOfHobbies array
  for (index = 0; index < lenOfData; index++) {
    let tempCityAndCountry = [];
    tempCityAndCountry[0] = data[index].city;
    tempCityAndCountry[1] = data[index].country;
    cityAndCountry[cityAndCountry.length] = tempCityAndCountry;
  }
  return cityAndCountry;
}
module.exports = logCityAndCountry;
