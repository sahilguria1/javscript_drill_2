function getFirstHobbies(data) {
    //handle edge cases
  if (data == null || data.length === 0) {
    return "provide some data to operate.";
  }
  const lenOfData = data.length;

  const hobbies = [];

  //using for loop to check the age and then putting hobbies in listOfHobbies array
  for (index = 0; index < lenOfData; index++) {
    if (data[index].hobbies.length > 0) {
      hobbies.push(data[index].hobbies[0]);
    }
  }
  return hobbies;
}
module.exports = getFirstHobbies;
