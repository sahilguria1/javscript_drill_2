//funtion check if person exist or not then return by if condition
function logNameAndCity(data, index) {
  //handle edge cases
  if (data == null || data.length === 0) {
    return "provide some data to operate.";
  }

  const person = data[index]; //getting data from array index
  if (person) {
    return  `${person.name}, ${person.city}`;

  } else {
    return "No person found at the given index";
  }
}
module.exports = logNameAndCity;
